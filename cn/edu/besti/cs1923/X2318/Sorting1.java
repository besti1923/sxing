package cn.edu.besti.cs1923.X2318;

import java.util.Arrays;
public class Sorting1
{
    public static  String selectionSort(int[] data) {
        int min;
        int temp;

        for (int index = 0; index < data.length - 1; index++) {
            min = index;
            for (int scan = index + 1; scan < data.length; scan++)
                if (data[scan]<(data[min]) )
                    min = scan;

            swap(data, min, index);
        }
        return Arrays.toString(data);
    }

    private static  void swap(int[] seed, int d1, int d2) {
        int temp = seed[d1];
        seed[d1] = seed[d2];
        seed[d2] = temp;
    }

}