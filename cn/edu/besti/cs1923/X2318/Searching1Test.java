package cn.edu.besti.cs1923.X2318;

import junit.framework.TestCase;

public class Searching1Test extends TestCase {

    public void testLinearSearch()
    {
        double test[] = {1, 7, 11, 9, 23};//正常
        assertEquals(true, Searching1.linearSearch(test,  1));//边界
        assertEquals(true, Searching1.linearSearch(test,  23));//边界
        assertEquals(true, Searching1.linearSearch(test,  9));//正常
        assertEquals(false, Searching1.linearSearch(test,  100));//异常


        double test2[] = {1, 2, 3, 4, 5};//正序
        assertEquals(true, Searching1.linearSearch(test2,  1));//边界
        assertEquals(true, Searching1.linearSearch(test2,  5));//边界
        assertEquals(true, Searching1.linearSearch(test2,  3));//正常
        assertEquals(false, Searching1.linearSearch(test2,  100));//异常

        double test3[] = {5, 4, 3, 2, 1};//倒序
        assertEquals(true, Searching1.linearSearch(test3,  1));//边界
        assertEquals(true, Searching1.linearSearch(test3,  5));//边界
        assertEquals(true, Searching1.linearSearch(test3,  3));//正常
        assertEquals(false, Searching1.linearSearch(test3,  100));//异常

        double test4[] = {2, 0, 1, 9, 2, 3, 1, 8};//正常
        assertEquals(true, Searching1.linearSearch(test4,  2));//边界
        assertEquals(true, Searching1.linearSearch(test4, 2));//边界
        assertEquals(true, Searching1.linearSearch(test4,  9));//正常
        assertEquals(false, Searching1.linearSearch(test4,  100));//异常

        double ARR5[] = {1, 2, 3, 2};//正序
        assertEquals(true, Searching1.linearSearch(ARR5, 1));//边界
        assertEquals(true, Searching1.linearSearch(ARR5, 2));//边界
        assertEquals(true, Searching1.linearSearch(ARR5, 2));//正常
        assertEquals(false, Searching1.linearSearch(ARR5,  100));//异常

        double test6[] = {2, 3, 2, 1};//倒序
        assertEquals(true, Searching1.linearSearch(test6, 2));//边界
        assertEquals(true, Searching1.linearSearch(test6,  1));//边界
        assertEquals(true, Searching1.linearSearch(test6, 3));//正常
        assertEquals(false, Searching1.linearSearch(test6,  100));//异常



    }
}