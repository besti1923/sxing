import java.util.LinkedList;
import java.util.Queue;
class Node<T> {
    Node left;
    Node right;
    T val;
    public Node() {
    }
    public Node(T val) {
        this.val = val;
    }
    public Node(T val, Node left, Node right) {
        this.val = val;
        this.left = left;
        this.right = right;
    }
}
public class BTree {
    private static char[] arr = {'A', 'B', '#', 'C', 'D', '#', '#', '#', 'E', '#', 'F', '#', '#'};
    private static int count = 0;
    public static Node Create() {
        if (count >= arr.length || arr[count] == '#') {
            count++;
            return null;        }
        Node node = new Node<>(arr[count++]);
        node.left = Create();
        node.right = Create();
        return node;
    }
    public static void levOrder(Node root) {
        if (root != null) {
            Node p = root;
            Queue<Node> queue = new LinkedList<>();
            queue.add(p);
            while (!queue.isEmpty()) {
                p = queue.poll();
                System.out.print(p.val + " ");
                if (p.left != null) {
                    queue.add(p.left);
                }
                if (p.right != null) {
                    queue.add(p.right);
                }
            }
        }
    }
    public static void main(String[] args) {
        System.out.println("\n输入为：AB#CD###E#F##");
        Node root = Create();
        System.out.println("\n层次遍历：");
        levOrder(root);
    }
}