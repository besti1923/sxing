import java.util.Scanner;

public class Rational
{
    public static void main (String[] args)
    {
        int n,m;
        char judge,count;
        Scanner scan = new Scanner (System.in);
        do
        {
            System.out.println ("please input +-*/%");
            count = scan.next().charAt(0);
            System.out.println ("please input n and m");
            n = scan.nextInt();
            m = scan.nextInt();
            switch (count)
            {
                case '+' :
                    System.out.println (n + "+" + m + "=" + (n+m));
                    break;
                case '-' :
                    System.out.println (n + "-" + m + "=" + (n-m));
                    break;
                case '*' :
                    System.out.println (n + "*" + m + "=" + (n*m));
                    break;
                case '/' :
                    System.out.println (n + "/" + m + "=" + (n/m));
                    break;
                case '%' :
                    System.out.println (n + "%" + m + "=" + (n%m));
                    break;
                default :
                    System.out.println ("error");
            }
            System.out.println ("do to y or leave to n ");
            judge = scan.next().charAt(0);
        }while(judge!='n');
    }
}
