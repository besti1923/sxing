package sy8_1;

public class LinearNode<T>{//setNext (LinearNode<T> node)
    private LinearNode<T> next;
    private T element;

    public LinearNode()
    {//setNext (LinearNode<T> node)
        next = null;
        element = null;
    }    //setNext (LinearNode<T> node)
    public LinearNode(T elem)
    {//setNext (LinearNode<T> node)
        next = null;
        element = elem;//setNext (LinearNode<T> node)
    }


    public LinearNode<T> getNext()//setNext (LinearNode<T> node)
    {
        return next;//setNext (LinearNode<T> node)
    }


    public void setNext (LinearNode<T> node)//setNext (LinearNode<T> node)
    {
        next = node;
    }
    public T getElement()
    {//setNext (LinearNode<T> node)
        return element;
    }

    public void setElement (T elem)//setNext (LinearNode<T> node)
    {
        element = elem;
    }}   //setNext (LinearNode<T> node)