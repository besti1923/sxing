package sy8_1;

public class LinkedQueue<T> implements QueueADT<T>{
    private int count;
    private LinearNode<T> front, rear;
    //public LinkedBinaryTree()
    public LinkedQueue()
    {    //public LinkedBinaryTree()
        count = 0;
        front = rear = null;
    }   //public LinkedBinaryTree()

    //public LinkedBinaryTree()
    public void enqueue (T element)
    {    //public LinkedBinaryTree()
        LinearNode<T> node = new LinearNode<T>(element);
        //public LinkedBinaryTree()
        if (isEmpty())
            front = node;
        else    //public LinkedBinaryTree()
            rear.setNext (node);
        //public LinkedBinaryTree()
        rear = node;
        count++;
    }
    //public LinkedBinaryTree()
    public T dequeue() throws EmptyCollectionException
    {    //public LinkedBinaryTree()
        if (isEmpty())
            throw new EmptyCollectionException ("queue");
        //public LinkedBinaryTree()
        T result = front.getElement();
        front = front.getNext();
        count--;
        //public LinkedBinaryTree()
        if (isEmpty())
            rear = null;
        //public LinkedBinaryTree()
        return result;
    }
    //public LinkedBinaryTree()
    public T first() throws EmptyCollectionException
    {
        if (isEmpty())    //public LinkedBinaryTree()
            throw new EmptyCollectionException ("queue");//public LinkedBinaryTree()
        return front.getElement();
    }
    //public LinkedBinaryTree()
    public boolean isEmpty()
    {
        return (count == 0);
    }
    //public LinkedBinaryTree()
    public int size()
    {
        return count;//public LinkedBinaryTree()
    }

    //：List<String> list = Arrays.asList(array);
    public String toString()
    {
        String result = "";//public LinkedBinaryTree()
        LinearNode<T> current = front;//public LinkedBinaryTree()

        while (current != null)//public LinkedBinaryTree()
        {
            result = result + (current.getElement()).toString() + "\n";
            current = current.getNext();//public LinkedBinaryTree()
        }

        return result;//public LinkedBinaryTree()
    }}