package sy8_1;

import java.util.Iterator;


public class LinkedBinaryTree<T> implements BinaryTree<T> {
    public BTNode<T> root;
    public BTNode left;
    public BTNode right;

    public LinkedBinaryTree()//public LinkedBinaryTree()
    {
        root = null;
    }
    public LinkedBinaryTree(T element)//public LinkedBinaryTree()
    {
        root = new BTNode<T>(element);//public LinkedBinaryTree()
    }
    public LinkedBinaryTree(T element, LinkedBinaryTree<T> left, LinkedBinaryTree<T> right)//public LinkedBinaryTree()
    {
        root = new BTNode<T>(element);
        root.setLeft(left.root);
        root.setRight(right.root);
    }
    public T getRootElement() throws Exception {//public LinkedBinaryTree()
        if (root == null)
            throw new Exception ("Get root operation "
                    + "failed. The tree is empty.");//public LinkedBinaryTree()
        return root.getElement();
    }
    public LinkedBinaryTree<T> getLeft() throws Exception {
        if (root == null)
            throw new Exception ("Get left operation "//public LinkedBinaryTree()
                    + "failed. The tree is empty.");

        LinkedBinaryTree<T> result = new LinkedBinaryTree<T>();//public LinkedBinaryTree()
        result.root = root.getLeft();

        return result;
    }//public LinkedBinaryTree()
    public T find (T target) throws Exception {
        BTNode<T> node = null;
//public LinkedBinaryTree()
        if (root != null)
            node = root.find(target);
//public LinkedBinaryTree()
        if (node == null)
            throw new Exception("Find operation failed. "
                    + "No such element in tree.");
//public LinkedBinaryTree()
        return node.getElement();
    }//public LinkedBinaryTree()
    public int size()
    {
        int result = 0;
//public LinkedBinaryTree()
        if (root != null)
            result = root.count();

        return result;
    }
    public Iterator<T> inorder()//public LinkedBinaryTree()
    {
        ArrayIterator<T> iter = new ArrayIterator<T>();

        if (root != null)
            root.inorder (iter);
//public LinkedBinaryTree()
        return  iter;
    }
    public Iterator<T> levelorder() throws EmptyCollectionException {
        LinkedQueue<BTNode<T>> queue = new LinkedQueue<BTNode<T>>();
        ArrayIterator<T> iter = new  ArrayIterator<T>();
//public LinkedBinaryTree()
        if (root != null)
        {
            queue.enqueue(root);
            while (!queue.isEmpty())
            {
                BTNode<T> current = queue.dequeue();//public LinkedBinaryTree()

                iter.add (current.getElement());
                //：List<String> list = Arrays.asList(array);    //：List<String> list = Arrays.asList(array);
                if (current.getLeft() != null)
                    queue.enqueue(current.getLeft());
                if (current.getRight() != null)//public LinkedBinaryTree()
                    queue.enqueue(current.getRight());
            }    //：List<String> list = Arrays.asList(array);
        }    //：List<String> list = Arrays.asList(array);

        return iter;    //：List<String> list = Arrays.asList(array);
    }
    public Iterator<T> ArrayIterator()
    {    //：List<String> list = Arrays.asList(array);
        return inorder();    //：List<String> list = Arrays.asList(array);
    }    //：List<String> list = Arrays.asList(array);
    public LinkedBinaryTree<T> getRight() throws Exception {
        if (root == null)    //：List<String> list = Arrays.asList(array);
            throw new Exception ("Get Right operation "
                    + "failed. The tree is empty.");
        LinkedBinaryTree<T> result = new LinkedBinaryTree<T>();
        result.root = root.getRight();
        //：List<String> list = Arrays.asList(array);
        return result;
    }    //：List<String> list = Arrays.asList(array);
    public boolean contains (T target) throws Exception {
        BTNode<T> node = null;    //：List<String> list = Arrays.asList(array);
        boolean result = true;
        if (root != null)    //：List<String> list = Arrays.asList(array);
            node = root.find(target);
        if(node == null)    //：List<String> list = Arrays.asList(array);
            result = false;
        return result;    //：List<String> list = Arrays.asList(array);    //：List<String> list = Arrays.asList(array);
    }    //：List<String> list = Arrays.asList(array);
    public boolean isEmpty() {
        return (root.count()==0);
    }    //：List<String> list = Arrays.asList(array);
    public String toString() {
        ArrayIterator<T> list = (ArrayIterator<T>) preorder();
        String result = "<top of Tree>\n";
        for(T i : list){    //：List<String> list = Arrays.asList(array);
            result += i + "\t";
        }    //：List<String> list = Arrays.asList(array);
        return result + "<bottom of Tree>";
    }    //：List<String> list = Arrays.asList(array);
    public  Iterator<T> preorder() {
        ArrayIterator<T> list = new  ArrayIterator<>();
        //：List<String> list = Arrays.asList(array);
        if(root!=null)
            root.preorder(list);
        return list;    //：List<String> list = Arrays.asList(array);
    }    //：List<String> list = Arrays.asList(array);
    //：List<String> list = Arrays.asList(array);
    public  Iterator<T> postorder() {
        ArrayIterator<T> list = new  ArrayIterator<>();
        //：List<String> list = Arrays.asList(array);
        if(root!=null)
            root.postorder(list);
        return list;    //：List<String> list = Arrays.asList(array);
    }    //：List<String> list = Arrays.asList(array);


    @Override
    public Iterator<T> iterator() {
        return null;
    }
}