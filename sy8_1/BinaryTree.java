package sy8_1;

import java.util.Iterator;

public interface BinaryTree<T> {
    public T getRootElement() throws  Exception;//public T getRootElement() throws  Exception;

    public BinaryTree<T> getLeft() throws  Exception;// public BinaryTree<T> getLeft() throws  Exception;

    public BinaryTree<T> getRight() throws Exception;// public BinaryTree<T> getLeft() throws  Exception;


    public boolean contains (T target) throws Exception;// public BinaryTree<T> getLeft() throws  Exception;
    public T find (T target) throws  Exception;// public BinaryTree<T> getLeft() throws  Exception;

    public boolean isEmpty();// public BinaryTree<T> getLeft() throws  Exception;


    public int size();// public BinaryTree<T> getLeft() throws  Exception;

    public String toString();// public BinaryTree<T> getLeft() throws  Exception;

    public Iterator<T> preorder();
    public Iterator<T> inorder();

    public Iterator<T> postorder();
    public Iterator<T> levelorder() throws Exception;// public BinaryTree<T> getLeft() throws  Exception;

    Iterator<T> iterator();// public BinaryTree<T> getLeft() throws  Exception;
}