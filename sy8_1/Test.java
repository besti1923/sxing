package sy8_1;

public class Test {
    public static void main(String[] args) {//public LinkedBinaryTree()
        LinkedBinaryTree<String> current = new LinkedBinaryTree<String>("2");
        BTNode<String> a = current.root;//public LinkedBinaryTree()
        a.left = new LinkedBinaryTree<String>("0").root;
        a.right = new LinkedBinaryTree<String>("2").root;//public LinkedBinaryTree()
        (a.left).left = new LinkedBinaryTree<String>("1").root;
        (a.left).right = new LinkedBinaryTree<String>("9").root;//public LinkedBinaryTree()
        (a.right).left = new LinkedBinaryTree<String>("3").root;//public LinkedBinaryTree()
        (a.right).right = new LinkedBinaryTree<String>("8").root;//public LinkedBinaryTree()
        ((a.right).left).right = new LinkedBinaryTree<String>("1").root;
        System.out.println("是否为空？");//public LinkedBinaryTree()
        System.out.println(current.isEmpty());
//public LinkedBinaryTree()
        ArrayIterator<String>Iterator = (ArrayIterator<String>) current.preorder();
        System.out.println("先序遍历：");//public LinkedBinaryTree()
        for(String i :Iterator){
            System.out.println(i);//public LinkedBinaryTree()
        }//public LinkedBinaryTree()
        ArrayIterator<String>Iterator2 = (ArrayIterator<String>) current.postorder();
        System.out.println("后序遍历：");//public LinkedBinaryTree()
        for(String i :Iterator2){//public LinkedBinaryTree()
            System.out.println(i);//public LinkedBinaryTree()
        }//public LinkedBinaryTree()
        ArrayIterator<String>Iterator3 = (ArrayIterator<String>) current.inorder();//public LinkedBinaryTree()
        System.out.println("中序遍历：");//public LinkedBinaryTree()
        for(String i :Iterator3){//public LinkedBinaryTree()
            System.out.println(i);//public LinkedBinaryTree()
        }//public LinkedBinaryTree()
    }//public LinkedBinaryTree()
}