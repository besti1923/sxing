package sy8_1;

public interface QueueADT<T>
{

    //public LinkedBinaryTree()
    public void enqueue (T element);

    public T dequeue() throws EmptyCollectionException;

    //public LinkedBinaryTree()
    public T first() throws EmptyCollectionException;

    //public LinkedBinaryTree()
    public boolean isEmpty();//：List<String> list = Arrays.asList(array);
    //public LinkedBinaryTree()
    public int size();
//public LinkedBinaryTree()

    public String toString();
}//public LinkedBinaryTree()