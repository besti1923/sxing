/*===============================================================
 *  编    者 : 20192318xs
 *  创作时间 : 2020年09月24日 星期四 08时40分06秒
 *  文件名称 : test2.java
 *  描    述 : 
================================================================*/

import java.util.Scanner;
public class test2
{
        public static void main (String[] args)
        {
                int n;
                int i,j;
                Scanner scan = new Scanner (System.in);
                do
                {
                        System.out.println ("enter n: ");
                        n = scan.nextInt();
                        if(n<=0)
                        {
                                System.out.println ("请重新输入");
                        }
                }while(n<=0);
                for(i=1;i<=n;i++)
                {
                        for(j=1;j<=i;j++)
                        {
                                System.out.print ("*");
                        }
                        System.out.print ("\n");
                }
        }
}
