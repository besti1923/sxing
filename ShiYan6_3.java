import java.io.*;
import java.util.Scanner;

public class ShiYan6_3
{
    public static void main(String[] args) throws IOException
    {
        ShiYan6_2_Link link = new ShiYan6_2_Link();
        Scanner scan = new Scanner(System.in);
        int n,a,b;

        File file = new File("ShiYan6_2.txt");
        if (!file.exists()){
            file.createNewFile();
        }
        OutputStream outputStream1 = new FileOutputStream(file);
        byte[] test1 = {'2','4'};
        outputStream1.write(test1);

        InputStream inputStream1 = new FileInputStream(file);
        a = (char) inputStream1.read()-48;
        b = (char) inputStream1.read()-48;

        System.out.print("请输入整数，输入完毕请以0结束：");
        for (;;)
        {
            n = scan.nextInt();
            if (n == 0)
            {
                break;
            }
            link.AddNode(n);
        }
        System.out.println("输入完毕！");
        System.out.print("输入的链表为：");
        link.Print();
        System.out.println("此时链表中的元素个数为："+link.Length());
        link.InsertNode(5,a);
        System.out.print("将数据a添加进链表中：");
        link.Print();
        System.out.println("此时链表中的元素个数为："+link.Length());
        link.InsertNode(3,b);
        System.out.print("将数据b添加进链表中：");
        link.Print();
        System.out.println("此时链表中的元素个数为："+link.Length());
        System.out.print("删除数据a：");
        link.DeleteNode(6);
        link.Print();
        System.out.println("此时链表中的元素个数为："+link.Length());
        link.Sort();
        System.out.print("使用选择排序对链表中的元素进行排序：");
        link.Print();
    }
}