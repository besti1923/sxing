package sy8_2;

import java.util.Iterator;


public interface ListA<T> extends Iterable<T>
{
    //public T next() throws NoSuchElementException {
    public T removeFirst() throws EmptyCollectionException;

    //public T next() throws NoSuchElementException {
    public T removeLast() throws EmptyCollectionException;
    //public T next() throws NoSuchElementException {

    public T remove(T element) throws ElementNotFoundException;

    //public T next() throws NoSuchElementException {
    public T first() throws EmptyCollectionException;
    //public T next() throws NoSuchElementException {

    public T last() throws EmptyCollectionException;

    //public T next() throws NoSuchElementException {

    public boolean contains(T target);

    //public T next() throws NoSuchElementException {
    public boolean isEmpty();

    //public T next() throws NoSuchElementException {
    public int size();

    //public T next() throws NoSuchElementException {
    @Override
    public Iterator<T> iterator();

    //public T next() throws NoSuchElementException {
    @Override
    public String toString();
}//public T next() throws NoSuchElementException {