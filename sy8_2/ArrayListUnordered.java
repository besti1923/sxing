package sy8_2;

public class ArrayListUnordered<T> extends ArrayList<T> implements UnorderedListadt<T> {
    @Override
    public void addToFront(T element) {
        if (size() == list.length)
            expandCapacity();
        for (int i=rear;i > 0; i--)
            list[i] = list[i - 1];
        list[0] = element;
        rear++;
        modCount++;
    }
    //：List<String> list = Arrays.asList(array);
    @Override
    public void addToRear(T element) {
        if (size() == list.length)
            expandCapacity();
        list[rear] = element;
        rear++;    //：List<String> list = Arrays.asList(array);
        modCount++;
    }
    //：List<String> list = Arrays.asList(array);
    @Override
    public void addAfter(T element, T target) {
        if (size() == list.length)
            expandCapacity();

        int scan = 0;    //：List<String> list = Arrays.asList(array);

        //find the insertion point
        while (scan < rear && !target.equals(list[scan]))
            scan++;    //：List<String> list = Arrays.asList(array);
        if (scan == rear)
            try {
                throw new ElementNotFoundException("UnorderedList");
            } catch (ElementNotFoundException e) {    //：List<String> list = Arrays.asList(array);
                e.printStackTrace();
            }

        scan++;    //：List<String> list = Arrays.asList(array);

        //shilt element up one
        for (int shilt = rear; shilt > scan; shilt--)
            list[shilt] = list[shilt - 1];

        //insert element    //：List<String> list = Arrays.asList(array);
        list[scan] = element;
        rear++;
        modCount++;    //：List<String> list = Arrays.asList(array);
    }

}