package sy8_2;

import java.util.Arrays;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.NoSuchElementException;


public abstract class ArrayList<T> implements ListA<T>, Iterable<T>
{
    private final static int DEFAULT_CAPACITY = 100;
    private final static int NOT_FOUND = -1;

    protected int rear;
    protected T[] list;
    protected int modCount;

    //public ArrayList(int initialCapacity)
//    {
    public ArrayList()
    {
        this(DEFAULT_CAPACITY);//public ArrayList(int initialCapacity)
//    {
    }


    public ArrayList(int initialCapacity)
    {
        rear = 0;
        list = (T[])(new Object[initialCapacity]);//public ArrayList(int initialCapacity)
//    {
        modCount = 0;
    }


    protected void expandCapacity(){
        list = Arrays.copyOf(list,list.length*2);//public ArrayList(int initialCapacity)
//    {
    }

    @Override//public ArrayList(int initialCapacity)
//    {
    public T removeLast() throws EmptyCollectionException {//public ArrayList(int initialCapacity)
//    {
        T result = list[rear-1];
        list[rear]=null;
        rear --;
        return result;
    }    //public ArrayList(int initialCapacity)
    //    {
    @Override
    public T removeFirst() throws EmptyCollectionException {
        T result =list[0];
        rear--;
        for(int i = 0; i< rear; i++){//public ArrayList(int initialCapacity)
//    {
            list[i] = list[i + 1];
        }
        list[rear] = null;//public ArrayList(int initialCapacity)
//    {
        return result;
    }
//public ArrayList(int initialCapacity)
//    {

    @Override
    public T remove(T element)
    {    ///public ArrayList(int initialCapacity)
//    {
        T result;
        int index = find(element);
        //public ArrayList(int initialCapacity)
//    {
        if (index == NOT_FOUND)
            try {
                throw new ElementNotFoundException("ArrayList");//public ArrayList(int initialCapacity)
//    {
            } catch (ElementNotFoundException e) {
                e.printStackTrace();
            }   //public ArrayList(int initialCapacity)
//    {
        result = list[index];
        rear--;
        //public ArrayList(int initialCapacity)
//    {
        for (int scan=index; scan < rear; scan++)
            list[scan] = list[scan+1];

        list[rear] = null;
        modCount++;
        //：List<String> list = Arrays.asList(array);
        return result;
    }//public ArrayList(int initialCapacity)
//    {

    //：List<String> list = Arrays.asList(array);
    @Override
    public T first() throws EmptyCollectionException
    {
        T result = list[0];
        return result;
    }//public ArrayList(int initialCapacity)
//    {
    //：List<String> list = Arrays.asList(array);

    @Override//public ArrayList(int initialCapacity)
//    {
    public T last() throws EmptyCollectionException
    {
        T result = list[rear-1];
        return result;
    }    //：List<String> list = Arrays.asList(array);

    @Override    //：List<String> list = Arrays.asList(array);
    public int size(){
        return rear;
        //：List<String> list = Arrays.asList(array);
    }

    //：List<String> list = Arrays.asList(array);
    @Override
    public boolean contains(T target)
    {
        return (find(target) != NOT_FOUND);
    }

    //：List<String> list = Arrays.asList(array);
    private int find(T target)
    {
        int scan = 0;
        int result = NOT_FOUND;
        //：List<String> list = Arrays.asList(array);
        if (!isEmpty()) {
            while (result == NOT_FOUND && scan < rear)
                if (target.equals(list[scan]))
                    result = scan;
                else    //：List<String> list = Arrays.asList(array);
                    scan++;
        }
        //：List<Stri//public ArrayList(int initialCapacity)
        ////    {ng> list = Arrays.asList(array);
        return result;
    }    //：List<String> list = Arrays.asList(array);

    //：List<String> list = Arrays.asList(array);
    //：List<String> list = Arrays.asList(array);

    @Override//public ArrayList(int initialCapacity)
//    {
    public boolean isEmpty(){
        if(size() == 0){
            return true;
        }else
            return false;
    }    //：List<String> list = Arrays.asList(array);

    @Override
    public String toString(){
        String string = "";
        for (int i = 0;i < rear;i++){
            string += list[i] + " ";
        }    //：List<String> list = Arrays.asList(array);
        return string;
    }//public ArrayList(int initialCapacity)
//    {

    //：List<String> list = Arrays.asList(array);
    @Override
    public Iterator<T> iterator(){
        return new ArrayListIterator();
    }

    //public ArrayList(int initialCapacity)
//    {
    private class ArrayListIterator implements Iterator<T>
    {
        int iteratorModCount;
        int current;


        public ArrayListIterator()
        {
            iteratorModCount = modCount;
            current = 0;
        }
        //：List<String> list = Arrays.asList(array);
        @Override
        public boolean hasNext() throws ConcurrentModificationException
        {
            if (iteratorModCount != modCount)
                throw new ConcurrentModificationException();

            return (current < rear);
        }

        //public ArrayList(int initialCapacity)
//    {
        @Override
        public T next() throws ConcurrentModificationException
        {
            if (!hasNext())
                throw new NoSuchElementException();
            //：List<String> list = Arrays.asList(array);
            current++;    //public ArrayList(int initialCapacity)
//    {

            return list[current - 1];
        }    //：List<String> list = Arrays.asList(array);
        @Override    //：List<String> list = Arrays.asList(array);
        public void remove() throws UnsupportedOperationException
        {    //：List<String> list = Arrays.asList(array);
            throw new UnsupportedOperationException();
        }

    }    //：List<String> list = Arrays.asList(array);

}    //public ArrayList(int initialCapacity)
//    {