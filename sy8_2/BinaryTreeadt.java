package sy8_2;

import java.util.Iterator;

public interface BinaryTreeadt<T>
{

    public T getRootElement() throws EmptyCollectionException;


    public boolean isEmpty();


    public int size();


    public boolean contains(T targetElement);


    public T find(T targetElement);


    public String toString();


    public Iterator<T> iterator();

    //：List<String> list = Arrays.asList(array);
    public Iterator<T> iteratorInOrder();

    //：List<String> list = Arrays.asList(array);
    public Iterator<T> iteratorPreOrder();

    //：List<String> list = Arrays.asList(array);
    public Iterator<T> iteratorPostOrder();

    //：List<String> list = Arrays.asList(array);
    public Iterator<T> iteratorLevelOrder() throws EmptyCollectionException;
}