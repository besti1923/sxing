package sy8_2;

import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.NoSuchElementException;


public class LinkedBinaryTree<T> implements Iterable<T>, BinaryTreeadt<T> {

    // 根结点的设置
    protected BinaryTreeNode<T> root;//根结点
    protected int modCount;// 修改标记 用于Iterator中使用
    protected LinkedBinaryTree<T> left,right;

    public LinkedBinaryTree() {
        root = null;
    }

    public LinkedBinaryTree(T element) {
        root = new BinaryTreeNode<T>(element);
    }

    public LinkedBinaryTree(T element, LinkedBinaryTree<T> left,
                            LinkedBinaryTree<T> right) {
        root = new BinaryTreeNode<T>(element);
        root.setLeft(left.root);    //：List<String> list = Arrays.asList(array);
        root.setRight(right.root);
        this.left = left;
        this.right=right;
    }
    //：List<String> list = Arrays.asList(array);
    //：List<String> list = Arrays.asList(array);
    public BinaryTreeNode<T> getRootNode() throws EmptyCollectionException {
        if (isEmpty()) {
            throw new EmptyCollectionException("BinaryTreeNode ");
        }    //：List<String> list = Arrays.asList(array);
        return root;
    }
    //public T next() throws NoSuchElementException {
    public LinkedBinaryTree<T> getLeft() {
        return left;
        // To be completed as a Programming Project
    }
    //public T next() throws NoSuchElementException {
    public LinkedBinaryTree<T> getRight() {
        return right;
    }

    //public T next() throws NoSuchElementException {

    public int getHeight()
    {
        return height(root);
    }

    //public T next() throws NoSuchElementException {
    private int height(BinaryTreeNode<T> node)
    {
        if(node==null){
            return 0;
        }
        else {
            int leftTreeHeight = height(node.getLeft());
            int rightTreeHeight= height(node.getRight());
            return leftTreeHeight>rightTreeHeight ? (leftTreeHeight+1):(rightTreeHeight+1);
        }
    }
    //：List<String> list = Arrays.asList(array);
    @Override
    public T getRootElement() throws EmptyCollectionException {
        if (root.getElement().equals(null)) {
            throw new EmptyCollectionException("BinaryTreeNode ");
        }    //：List<String> list = Arrays.asList(array);
        return root.getElement();
    }
    //：List<String> list = Arrays.asList(array);
    @Override
    public boolean isEmpty() {
        return (root == null);
    }

    //public T next() throws NoSuchElementException {
    @Override
    public int size() {

        int size = 0;
        if(root.getLeft()!=null){
            size+=1;
        }    //：List<String> list = Arrays.asList(array);
        if(root.getRight()!=null){
            size+=1;
        }    //public T next() throws NoSuchElementException {
        return size;
    }

    //删除某结点的右侧部分
    public String removeRightSubtree() throws EmptyCollectionException {
        if (root == null)    //：List<String> list = Arrays.asList(array);
            throw new EmptyCollectionException("tree is empty");
        BinaryTreeNode cur = root;    //：List<String> list = Arrays.asList(array);
        while (cur.getLeft() != null){
            cur.setRight(null);    //：List<String> list = Arrays.asList(array);
            cur = cur.left;
        }    //：List<String> list = Arrays.asList(array);
        return super.toString();
    }
    //public T next() throws NoSuchElementException {
    public void removeAllElements() throws EmptyCollectionException {
        if (root == null)
            throw new EmptyCollectionException("tree is empty");
        root = null;
    }  //public T next() throws NoSuchElementException {

    @Override    //：List<String> list = Arrays.asList(array);
    public boolean contains(T targetElement) {
        if(targetElement == find(targetElement))
            return true;
        else    //：List<String> list = Arrays.asList(array);
            return false;
    }   //public T next() throws NoSuchElementException {

    @Override
    public T find(T targetElement) {
        // 获取当前元素
        BinaryTreeNode<T> current = findAgain(targetElement, root);
        //public T next() throws NoSuchElementException {
        if (current == null)
            try {
                throw new ElementNotFoundException("LinkedBinaryTree");
            } catch (ElementNotFoundException e) {
                e.printStackTrace();
            }  //public T next() throws NoSuchElementException {
        return (current.getElement());
    }
    //：List<String> list = Arrays.asList(array);
    private BinaryTreeNode<T> findAgain(T targetElement, BinaryTreeNode<T> next) {
        if (next == null)
            return null;
        //public T next() throws NoSuchElementException {
        if (next.getElement().equals(targetElement))
            return next;    //：List<String> list = Arrays.asList(array);
        // 递归调用
        BinaryTreeNode<T> temp = findAgain(targetElement, next.getLeft());

        if (temp == null)
            temp = findAgain(targetElement, next.getRight());

        return temp;    //：List<String> list = Arrays.asList(array);
    }
    //public T next() throws NoSuchElementException {
    @Override
    public Iterator<T> iteratorInOrder() {
        ArrayListUnordered<T> tempList = new ArrayListUnordered<T>();
        inOrder(root, tempList);
        return new TreeIterator(tempList.iterator());
    }    //：List<String> list = Arrays.asList(array);
    //public T next() throws NoSuchElementException {
    public void toPreString(){
        preOrder(root);
    }    //：List<String> list = Arrays.asList(array);
    private void preOrder(BinaryTreeNode root){
        if(null!= root){    //：List<String> list = Arrays.asList(array);
            System.out.print(root.getElement() + "\t");
            preOrder(root.getLeft());
            preOrder(root.getRight());
        }    //：List<String> list = Arrays.asList(array);
    }    //：List<String> list = Arrays.asList(array);
    //public T next() throws NoSuchElementException {
    public void toPostString(){
        postOrder(root);
    }
    private void postOrder(BinaryTreeNode root) {
        if (null != root) {
            postOrder(root.getLeft());
            postOrder(root.getRight());
            System.out.print(root.getElement() + "\t");
        }
    }

    //public T next() throws NoSuchElementException {
    protected void inOrder(BinaryTreeNode<T> node, ArrayListUnordered<T> tempList) {
        if (node != null) {
            inOrder(node.getLeft(), tempList);
            tempList.addToRear(node.getElement());
            inOrder(node.getRight(), tempList);
        }
    }
    //public T next() throws NoSuchElementException {
    @Override
    public Iterator<T> iteratorPreOrder() {
        ArrayListUnordered<T> tempList = new ArrayListUnordered<T>();
        preOrder(root, tempList);
        return new TreeIterator(tempList.iterator());
    }
    private void preOrder(BinaryTreeNode<T> node, ArrayListUnordered<T> tempList) {
        if (node != null) {
            tempList.addToRear(node.getElement());
            inOrder(node.getLeft(), tempList);
            inOrder(node.getRight(), tempList);
        }
    }
    //public T next() throws NoSuchElementException {
    @Override
    //为树的后序遍历返回一个迭代器
    public Iterator<T> iteratorPostOrder() {
        ArrayListUnordered<T> tempList = new ArrayListUnordered<T>();
        postOrder(root, tempList);
        return new TreeIterator(tempList.iterator());
    }
    private void postOrder(BinaryTreeNode<T> node, ArrayListUnordered<T> tempList) {
        if (node != null) {
            tempList.addToRear(node.getElement());
            inOrder(node.getLeft(), tempList);
            inOrder(node.getRight(), tempList);
        }
    }

    //public T next() throws NoSuchElementException {
    @Override
    public Iterator<T> iteratorLevelOrder() throws EmptyCollectionException {
        ArrayListUnordered<BinaryTreeNode<T>> nodes = new ArrayListUnordered<BinaryTreeNode<T>>();
        ArrayListUnordered<T> tempList = new ArrayListUnordered<T>();
        BinaryTreeNode<T> current;

        nodes.addToRear(root);

        while (!nodes.isEmpty()) {
            current = nodes.removeFirst();

            if (current != null) {
                tempList.addToRear(current.getElement());
                System.out.println(current.element);
                if (current.getLeft() != null)
                    nodes.addToRear(current.getLeft());
                System.out.println(current.left);
                if (current.getRight() != null)
                    nodes.addToRear(current.getRight());
                System.out.println(current.right);
            } else
                tempList.addToRear(null);
        }

        return new TreeIterator(tempList.iterator());
    }
    //public T next() throws NoSuchElementException {
    public void toLevelString1(){
        if(root == null)
            return;
        int height = getHeight();
        for(int i = 1; i <= height; i++){
            levelOrder(root,i);
        }
    }//public T next() throws NoSuchElementException {
    private void levelOrder(BinaryTreeNode root,int level){
        if(root == null || level < 1){
            return;
        }
        if(level == 1){
            System.out.print(root.getElement() + "\n");
            return;
        }
        levelOrder(root.getLeft(),level - 1);
        levelOrder(root.getRight(),level - 1);
    }//public T next() throws NoSuchElementException {

    @Override
    public Iterator<T> iterator() {
        return iteratorInOrder();
    }//public T next() throws NoSuchElementException {

    public String toString() {//public T next() throws NoSuchElementException {
        UnorderedListadt<BinaryTreeNode<T>> nodes = new ArrayListUnordered<BinaryTreeNode<T>>();
        UnorderedListadt<Integer> levelList = new ArrayListUnordered<Integer>();//public T next() throws NoSuchElementException {
//public T next() throws NoSuchElementException {
        BinaryTreeNode<T> current = null;
        String result = "";
        int printDepth = this.getHeight();
        int possibleNodes = (int) Math.pow(2, printDepth + 1);//public T next() throws NoSuchElementException {
        int countNodes = 0;

        nodes.addToRear(root);//public T next() throws NoSuchElementException {
        Integer currentLevel = 0;
        Integer previousLevel = -1;
        levelList.addToRear(currentLevel);//public T next() throws NoSuchElementException {

        while (countNodes < possibleNodes) {//public T next() throws NoSuchElementException {
            countNodes = countNodes + 1;
            try {//public T next() throws NoSuchElementException {
                current = nodes.removeFirst();//public T next() throws NoSuchElementException {
            } catch (EmptyCollectionException e) {
                e.printStackTrace();//public T next() throws NoSuchElementException {
            }
            try {//public T next() throws NoSuchElementException {
                currentLevel = levelList.removeFirst();
            } catch (EmptyCollectionException e) {
                e.printStackTrace();
            }
            if (currentLevel > previousLevel) {
                result = result + "\n\n";
                previousLevel = currentLevel;//public T next() throws NoSuchElementException {
                for (int j = 0; j < ((Math.pow(2, (printDepth - currentLevel))) - 1); j++)
                    result = result + " ";
            } else {//public T next() throws NoSuchElementException {
                for (int i = 0; i < (Math.pow(2, (printDepth - currentLevel + 1)) - 1); i++) {
                    result = result + " ";//public T next() throws NoSuchElementException {
                }
            }
            if (current != null) {
                result = result + (current.getElement()).toString();//public T next() throws NoSuchElementException {
                nodes.addToRear(current.getLeft());
                levelList.addToRear(currentLevel + 1);//public T next() throws NoSuchElementException {
                nodes.addToRear(current.getRight());
                levelList.addToRear(currentLevel + 1);//public T next() throws NoSuchElementException {
            } else {
                nodes.addToRear(null);
                levelList.addToRear(currentLevel + 1);//public T next() throws NoSuchElementException {
                result = result + " ";
            }
        }
        return result;
    }
    //public T next() throws NoSuchElementException {
    private class TreeIterator implements Iterator<T> {
        private int expectedModCount;
        private Iterator<T> iter;
        //public T next() throws NoSuchElementException {
        public TreeIterator(Iterator<T> iter) {
            this.iter = iter;
            expectedModCount = modCount;
        }
        //public T next() throws NoSuchElementException {
        public boolean hasNext() throws ConcurrentModificationException {
            if (!(modCount == expectedModCount))
                throw new ConcurrentModificationException();

            return (iter.hasNext());
        }

        //public T next() throws NoSuchElementException {
        public T next() throws NoSuchElementException {//public T next() throws NoSuchElementException {
            if (hasNext())
                return (iter.next());
            else
                throw new NoSuchElementException();
        }

        public void remove() {
            throw new UnsupportedOperationException();
        }
    }

}