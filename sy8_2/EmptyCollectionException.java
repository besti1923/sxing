package sy8_2;

public class EmptyCollectionException extends Exception {
    public EmptyCollectionException(String collection)
    {    //：List<String> list = Arrays.asList(array);
        super("The " + collection + " is empty.");
    }    //：List<String> list = Arrays.asList(array);
}