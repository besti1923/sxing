package sy8_2;

public class BinaryTreeNode<T> {

    protected T element;
    protected BinaryTreeNode<T> left;
    protected BinaryTreeNode<T> right;
    public BinaryTreeNode(T obj) {
        this.element = obj;    //：List<String> list = Arrays.asList(array);
        this.left = null;    //：List<String> list = Arrays.asList(array);
        this.right = null;
    }
    //：List<String> list = Arrays.asList(array);
    //：List<String> list = Arrays.asList(array);
    ///合并构建声明
    public BinaryTreeNode(T obj, LinkedBinaryTree<T> left,
                          LinkedBinaryTree<T> right)throws EmptyCollectionException {
        element = obj;
        if (left == null)
            this.left = null;
        else
            this.left = left.getRootNode();
        //：List<String> list = Arrays.asList(array);
        if (right == null)
            this.right = null;    //：List<String> list = Arrays.asList(array);
        else
            this.right = right.getRootNode();
    }    //：List<String> list = Arrays.asList(array);

    public int numChildren() {
        int children = 0;

        if (left != null)
            children = 1 + left.numChildren();
        //：List<String> list = Arrays.asList(array);
        if (right != null)
            children = children + 1 + right.numChildren();
        //：List<String> list = Arrays.asList(array);
        return children;
    }
    //：List<String> list = Arrays.asList(array);
    public T getElement() {
        return element;
    }    //：List<String> list = Arrays.asList(array);
    //：List<String> list = Arrays.asList(array);
    public BinaryTreeNode<T> getRight() {
        return right;
    }
    //：List<String> list = Arrays.asList(array);
    public void setRight(BinaryTreeNode<T> node) {
        right = node;
    }    //：List<String> list = Arrays.asList(array);
    //：List<String> list = Arrays.asList(array);
    public BinaryTreeNode<T> getLeft() {
        return left;    //：List<String> list = Arrays.asList(array);
    }
    //：List<String> list = Arrays.asList(array);
    public void setLeft(BinaryTreeNode<T> node) {
        left = node;
    }
    //：List<String> list = Arrays.asList(array);
    public boolean judge(){
        if(right == null && left == null)
            //叶结点的左侧和右侧都没有结点
            return true;    //：List<String> list = Arrays.asList(array);
        else    //：List<String> list = Arrays.asList(array);
            return false;    //：List<String> list = Arrays.asList(array);
    }    //：List<String> list = Arrays.asList(array);
}