package sy7_1;

public class Searching1
{
    public static boolean linearSearch(double[] seed, double obj)
    {
        int i;
        double[] a = new double[100];
        a[0] = obj;

        for (i = 1; i <=seed.length; i++) {
            a[i] = seed[i - 1];
        }

        while (a[i]!= obj)
        {
            i--;
        }

        if (i == 0)
            return false;
        else
            return true;
    }
}