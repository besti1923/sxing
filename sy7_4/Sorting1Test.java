package sy7_4;

import junit.framework.TestCase;

import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class Sorting1Test extends TestCase {

    public void testselectionSort() {
        String expect = "[18, 19, 20, 23]";
        String expect2 = "[1, 2, 3, 4, 5]";

        String expect3 = "[18, 3, 2, 1]";
        String expect4 = "[1, 2, 3, 18]";



        Comparable test[] = {20,19,23,18,};//
        assertEquals(expect, Sorting1.selectionSort(test));//
        assertNotEquals(expect2,Sorting1.selectionSort(test));//

        Comparable test2[] = {1,2,3,4,5};//
        assertEquals(expect2,Sorting1.selectionSort(test2));//
        assertNotEquals(expect,Sorting1.selectionSort(test2));//

        Comparable test3[] = {5,4,3,2,1};//
        assertEquals(expect2,Sorting1.selectionSort(test3));//
        assertNotEquals(expect,Sorting1.selectionSort(test3));//

        Comparable test4[] = {2, 3, 1, 18};//
        assertEquals(expect4,Sorting1.selectionSort(test4));//
        assertNotEquals(expect3,Sorting1.selectionSort(test4));//

        Comparable test5[] = {1, 2, 3, 18};//
        assertEquals(expect4,Sorting1.selectionSort(test5));//
        assertNotEquals(expect3,Sorting1.selectionSort(test5));//

        Comparable test6[] = {18, 3, 2, 1};//
        assertEquals(expect4,Sorting1.selectionSort(test6));//
        assertNotEquals(expect3,Sorting1.selectionSort(test6));//


    }
    public void testinsertionSort()
    {
        Comparable[] except = {0,1,2,3,4,5,6,7,8,9,2318};
        Comparable[] test = {2318,9,8,7,6,5,4,3,2,1,0};
        Sorting1.insertionSort(test);
        assertEquals(except[0],test[0]);
    }

    public void testguluguluSort() {
        Comparable[] except = {0,1,2,3,4,5,6,7,8,9,2318};
        Comparable[] test = {2318,9,8,7,6,5,4,3,2,1,0};
        Sorting1.guluguluSort(test);
        assertEquals(except[1],test[0]);

    }



    public void testquickSort() {
        Comparable[] except= {1,2,3,4,5,6,7,8,9,2318};
        Comparable[] test = {2318,9,8,7,6,5,4,3,2,1};
        Sorting1.QuickSort(test,0,9);
        assertEquals(except[1],test[0]);

    }




    public void testshellSort() {
        Comparable[] except = {0,1,2,3,4,5,6,7,8,9,2318};
        Comparable[] test = {2318,9,8,7,6,5,4,3,2,1,0};

        Sorting1.ShellSort(test,11);
        assertEquals(except[0],test[0]);

    }



}