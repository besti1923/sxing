package sy8_3;

public class DecideTree {
    protected BTNode r;
    protected String s;
    public DecideTree(){
        r=new BTNode("学号");
        r.left=new BTNode("20192318");
        BTNode one=r.left;
        r.right=new BTNode("宿舍");
        one.left=new BTNode("育才211");
        one.right=new BTNode("体温");
        BTNode two=one.left;
        two.left=new BTNode("36.5");
        two.right=new BTNode("体温正常");
    }
    public void run(){
        System.out.println(r.element);
    }
    public int run(String a) {
        String str = a;
        switch (str) {
            case "y": {
                r = r.left;
                break;
            }
            case "n": {
                r = r.right;
                break;
            }
            default:
                System.out.println("illegal input");
        }
        if (r != null) {
            System.out.println(r.element);
            return 1;
        }
        else
            return -1;
    }
}