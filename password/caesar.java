package password;

public class caesar
{
    public static void main(String args[]) throws Exception{
        String s=args[0];//取输入的第一个字符串，即需要加密的明文
        int key=Integer.parseInt(args[1]);//取输入的第二个字符串，即密钥，并将其转为整型
        String es="";//创建接收密文的字符串变量并初始化
        for(int i=0;i<s.length( );i++)//一个个取明文的字符并逐一根据密钥进行加密
        {  char c=s.charAt(i);
            if(c>='a' && c<='z') // 是小写字母
            { c+=key%26;  //移动key%26位
                if(c<'a') c+=26;  //向左超界
                if(c>'z') c-=26;  //向右超界
            }
            else if(c>='A' && c<='Z') // 是大写字母
            {  c+=key%26;
                if(c<'A') c+=26;
                if(c>'Z') c-=26;
            }
            es+=c;
        }
        System.out.println(es);
    }
}
