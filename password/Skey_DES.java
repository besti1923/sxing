package password;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
public class Skey_DES{
    public static void main(String args[])
            throws Exception{

        KeyGenerator kg=KeyGenerator.getInstance("DESede");
//这一行是创建一个密钥生成器，指定使用"DESede"算法
//与其它类的对象的创建方法不同，KeyGenerator通过它的静态方法getInstance()来创建对象

        kg.init(168);
        //初始化密钥生成器kg

        SecretKey k=kg.generateKey( );
        //生成密钥。
        //Keygenerator类中的generateKey方法可以生成密钥，类型是SecretKey，可用于后面的加密解密

        FileOutputStream  f=new FileOutputStream("key1.dat");
        //创建一个FileOutputStream类f，同时创建并让f指向一个新文件"key1.dat"

        ObjectOutputStream b=new  ObjectOutputStream(f);
        //创建一个新的ObjectOutputStream类的对象

        b.writeObject(k);
        //将生成的密钥k序列化后保存在"key1.dat"文件中
    }
}