import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.StringTokenizer;


public class Server {

    private static Fraction frac2;
    private static Fraction frac1;
    private static String a, b;
    private static char ch;
    private static Fraction result = null;

    public static void main(String[] args) throws IOException {
        //1.建立一个服务器Socket(ServerSocket)绑定指定端口
        ServerSocket serverSocket = new ServerSocket(8809);
        //2.使用accept()方法阻止等待监听，获得新连接
        Socket socket = serverSocket.accept();
        //3.获得输入流
        InputStream inputStream = socket.getInputStream();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        //获得输出流
        OutputStream outputStream = socket.getOutputStream();
        PrintWriter printWriter = new PrintWriter(outputStream);
        //4.读取用户输入信息
        String info = null;
        System.out.println("服务器已经建立......");
        while (!((info = bufferedReader.readLine()) == null)) {
            System.out.println("我是服务器，用户信息为：" + info);


            StringTokenizer st = new StringTokenizer(info, " ", false);
            a = st.nextToken();
            ch = st.nextToken().charAt(0);
            b = st.nextToken();
            frac1 = new Fraction(a);
            frac2 = new Fraction(b);

            switch (ch) {
                case '+':
                    result = frac1.getJia(frac2);

                    break;
                case '-':
                    result = frac1.getJian(frac2);

                    break;
                case '*':
                    result = frac1.getCheng(frac2);

                    break;
                case '/':
                    result = frac1.getChu(frac2);

                    break;
                default:

                    break;
            }
        }


        //给客户一个响应
        String reply = frac1 + String.valueOf(ch) + frac2 + "=" + result;
        printWriter.write(reply);
        printWriter.flush();
        //5.关闭资源
        printWriter.close();
    }
}