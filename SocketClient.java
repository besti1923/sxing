package yunbanke.Server;

import java.io.*;
import java.net.Socket;

public class SocketClient {
    public static void main(String[] args) throws IOException {
        Socket socket = new Socket("192.168.1.241",4567);
        OutputStream outputStream = socket.getOutputStream();
        PrintWriter printWriter = new PrintWriter(outputStream);
        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream);
        InputStream inputStream = socket.getInputStream();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream,"GBK"));
        String info1 = " 用户名：SXing，密码：112233445566";
        String info = new String(info1.getBytes("GBK"),"GBK");
        printWriter.write(info);
        printWriter.flush();
        outputStreamWriter.write(info1);
        outputStreamWriter.flush();
        socket.shutdownOutput();
        String reply = null;
        while (!((reply = bufferedReader.readLine()) ==null)){
            System.out.println("接收服务器的信息为：" + reply);
        }
        bufferedReader.close();
        inputStream.close();
        outputStreamWriter.close();
        printWriter.close();
        outputStream.close();
        socket.close();
    }
}