package two;

public class Searching {
    public static void main(String[] args)
    {
        int a[]={2,6,12,25,38,42,51,67,73,89,99};
        int result=BoundarySearching(a,0,a.length-1,73);
        System.out.println("所在位置是："+result);
    }
    public static int BoundarySearching(int[] a, int start, int end,int key) {
        if(a == null) {
            return -1;
        }
        if(start > end) {
            return -1;
        }
        int mid = (end - start)/2 + start;
        if(a[mid] > key) {
            return BoundarySearching(a, start, mid-1, key);
        } else if(a[mid] < key) {
            return BoundarySearching(a, mid+1, end, key);
        } else {
            return mid;
        }
    }
}