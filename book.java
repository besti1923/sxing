/*===============================================================
 *  编    者 : 20192318xs
 *  创作时间 : 2020年09月28日 星期一 21时32分39秒
 *  文件名称 : book.java
 *  描    述 : 
================================================================*/


public class book
{
        String name;
        String writer;
        double price;
        public String setname (String name)
        {
                this.name = name;
                return this.name;
        }
        public String setwriter (String writer)
        {
                this.writer = writer;
                return this.writer;
        }
        public double setprice (double price)
        {
                this.price = price;
                return this.price;
        }
        public book ()
        {
                this.name = "";
                this.writer = "";
                this.price = 0;
        }
        public book (String name,String writer,double price)
        {
                this.name = name;
                this.writer = writer;
                this.price = price;
        }
}
