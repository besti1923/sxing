import java.lang.*;
public class Complex {
    public void setRealpart(double R){
        RealPart=R;
    }
    public double getRealpart(){
        return RealPart;
    }
    public void setImagePart(double I){
        ImagePart=I;
    }
    public double getImagePart(){
        return ImagePart;
    }

    double RealPart;
    double ImagePart;
    public Complex(double R,double I){
        RealPart=R;
        ImagePart=I;
    }
    public boolean equals(Complex obj){
        if(this.getRealpart()==obj.getRealpart() && this.getImagePart()==obj.getImagePart())
            return true;
        else
            return false;
    }
    public String toString(){
        return RealPart+"+"+ImagePart+"i";
    }

    public Complex ComplexAdd(Complex a){
        return new Complex(this.RealPart+a.RealPart,this.ImagePart+a.ImagePart);
    }
    public Complex ComplexSub(Complex a){
        return new Complex(this.RealPart-a.RealPart,this.ImagePart-a.ImagePart);
    }
    public Complex ComplexMulti(Complex a){
        return new Complex(this.RealPart*a.RealPart-this.ImagePart*a.ImagePart,
                this.RealPart*a.ImagePart+this.ImagePart*a.RealPart);
    }
    public Complex ComplexDiv(Complex a){
        double x=this.RealPart;
        double y=this.ImagePart;
        double m=a.RealPart;
        double n=a.ImagePart;
        return new Complex((x*m+y*n)/(m*m+n*n),(y*m-x*n)/(m*m+n*n));
    }
}