public class ShiYan6_2_Link
{
    ShiYan6_2_Node head = null;
    int xs20192318 = 0;

    public void AddNode(int d)
    {
        ShiYan6_2_Node node = new ShiYan6_2_Node(d);
        if (head == null) {
            head = node;
            xs20192318++;
            return;
        }

        ShiYan6_2_Node temp = head;

        while (temp.next != null)
        {
            temp = temp.next;
        }
        temp.next = node;
        xs20192318++;
    }

    public void InsertNode(int index1,int d)
    {
        if (index1 > xs20192318||index1 < 0)
        {
            System.out.println("超出范围！");
            return;
        }

        int index = index1 - 1;
        ShiYan6_2_Node node = new ShiYan6_2_Node(d);

        ShiYan6_2_Node temp = head;
        int i;
        for(i = 0;i < index-1;i++)
        {
            temp = temp.next;
        }
        node.next = temp.next;
        temp.next = node;
        xs20192318++;
    }

    public boolean DeleteNode(int index)
    {
        if (index < 1 || index > Length())
        {
            System.out.println("超出范围！");
            return false;
        }
        if (index == 1)
        {
            head = head.next;
            xs20192318--;
            return true;
        }
        int i = 1;
        ShiYan6_2_Node preNode = head;
        ShiYan6_2_Node curNode = preNode.next;
        while (curNode != null)
        {
            if (i == index-1)
            {
                preNode.next = curNode.next;
                xs20192318--;
                return true;
            }
            preNode = curNode;
            curNode = curNode.next;
            i++;
        }
        return false;
    }

    public int Length()
    {
        return xs20192318;
    }

    public boolean DeleteNode1(ShiYan6_2_Node n)
    {
        if (n == null || n.next == null)
        {
            return false;
        }
        int tmp = n.data;
        n.data = n.next.data;
        n.next.data = tmp;
        n.next = n.next.next;
        System.out.println("删除成功！");
        return true;
    }

    public void Print()
    {
        ShiYan6_2_Node temp1 = head;
        while (temp1 != null)
        {
            System.out.print(temp1.data+" ");
            temp1 = temp1.next;
        }
        System.out.println();
    }

    public void Sort()
    {
        ShiYan6_2_Node newHead = head;
        int temp;
        ShiYan6_2_Node node = head;
        while (node != null) {
            ShiYan6_2_Node reNode = node.next;
            while (reNode != null) {
                if (reNode.data < node.data) {
                    temp = reNode.data;
                    reNode.data = node.data;
                    node.data = temp;
                }
                reNode = reNode.next;
            }
            node = node.next;
        }
    }
}